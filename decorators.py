from logging import getLogger


logger = getLogger("chromeheadless")

def func_log(func):

    def wrapper(*args, **kwargs):
        elems = []
        if args:
            elems.append(args)
        if kwargs:
            elems.append(kwargs)
        msg = 'chromeheadless.BrowserPage :: {} :: {}'.format(func.__name__, elems[1:])
        logger.warn(msg)
        return func(*args, **kwargs)

    return wrapper
